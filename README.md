# Platform Manager plugin

Base class for pfm plugins

## Configuration defaults

Base configuration expects:

    debug: bool  # set debug level for logs
    rabbitmq:
        url: string  # connection string to rabbitmq
    pfm:
        url: string  # url to pfm traefik proxy
        secret: string  # JWT secret shared with pfm

This config can be overriden by env variables:

* PFM_LOG_DEBUG: 0 or 1
* PFM_RABBITMQ_URL
* PFM_URL
* PFM_SECRET

## Development

Create a class extending PfmPlugin then:

    class PfmEvent(PfmPlugin):
        '''
        Example plugin
        '''

        def __init__(self, config:dict):
            super().__init__("myplugin", config)
    

    config = {
        debug: true,
        rabbitmq: {
            url: 'amqp://user:pwd@127.0.0.1:5672/%2F'
        },
        pfm: {
            url: 'http://localhost:8081',
            secret: 'mysecret'
        }
    }
    my_plugin = PfmEvent(config)
    rocket_plugin.run()
