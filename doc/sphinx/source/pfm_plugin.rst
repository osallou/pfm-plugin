pfm\_plugin package
===================

Submodules
----------

pfm\_plugin.pfm module
----------------------

.. automodule:: pfm_plugin.pfm
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pfm_plugin
   :members:
   :undoc-members:
   :show-inheritance:
