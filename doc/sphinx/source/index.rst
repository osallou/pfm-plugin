.. pfm-plugin documentation master file, created by
   sphinx-quickstart on Wed Dec  2 14:55:50 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pfm-plugin's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules
   pfm_plugin

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
