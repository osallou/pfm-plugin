"""
Base class to create pfm plugins
"""
import os
import json
import logging
import jwt
import requests
import pika
import signal

class PfmPlugin(object):

    def __override_defaults(self, config:dict) -> dict:
        '''
        Override config with environment variables

        Supported variables:
          * PFM_LOG_DEBUG
          * PFM_RABBITMQ_URL
          * PFM_URL
          * PFM_SECRET

        Args:
            config: configuration info
        Returns:
            updated configuration

        '''
        if os.environ.get('PFM_LOG_DEBUG', None):
            config['debug'] = True if os.environ['PFM_LOG_DEBUG'] == '1' else False
        if os.environ.get('PFM_RABBITMQ_URL', None):
            config['rabbitmq']['url'] = os.environ['PFM_RABBITMQ_URL']
        if os.environ.get('PFM_URL', None):
            config['pfm']['url'] = os.environ['PFM_URL']
        if os.environ.get('PFM_SECRET', None):
            config['pfm']['secret'] = os.environ['PFM_SECRET']
        return config

    def __init__(self, name:str, config:dict):
        '''
        Constructor for a plugin

        Args:
            name: name of the plugin
            config: input configuration
        '''
        self.config = self.__override_defaults(config)
        self.name = name
        self.logger = logging.getLogger('pfm_plugin')
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        if config.get('debug', False):
            ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)
        if config.get('debug', False):
            self.logger.setLevel(logging.DEBUG)
            self.logger.warning('[plugin=%s] set debug logs' % self.name)
        else:
            self.logger.setLevel(logging.INFO)
        self.logger.info("new event plugin for %s" % self.name)
        self.pfm = config['pfm']['url']
        self.secret = config['pfm']['secret']

        parameters = pika.URLParameters(config['rabbitmq']['url'])
        connection = pika.BlockingConnection(parameters)
        self.channel = connection.channel()
        self.plugin_queue = self.channel.queue_declare(
            queue='pfm_plugin_%s' % self.name,
            durable=True
        )
        self.channel.exchange_declare(
            exchange='pfm_events',
            exchange_type='fanout'
        )
        self.channel.queue_bind(
            exchange='pfm_events',
            queue=self.plugin_queue.method.queue
        )
        self.should_terminate = False
        self.consumer_tag = None

    def exit_gracefully(self, signum, frame):
        self.logger.warning('[plugin=%s] received sigterm' % self.name)
        self.should_terminate = True

    def call(self, space:int, module_name:str, func_name:str, data:dict) -> dict:
        '''
        Method to call a pfm module and function

        Args:
            space: space identifier
            module_name: name of the module
            func_name: function to call
            data: POST'ed data related to module/function
        Returns:
            call data, None in case of error

        '''
        headers = {
            'X-PFM-PLUGIN': self.name,
            'X-PFM-PLUGINSECRET': self.token(),
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
        r = requests.post(
            '%s/internal/api/pfm/space/%d/admin/module/%s/%s' % (
                self.pfm,
                space,
                module_name,
                func_name
            ),
            json=data,
            headers=headers
        )
        if r.status_code != 200:
            self.logger.error('[plugin=%s][space=%d] %s/%s error %d: %s' % (
                self.name,
                space,
                module_name,
                func_name,
                r.status_code,
                r.text
            ))
            return None
        result_data = r.json()
        return result_data

    def get_space_users(self, space:int) -> dict:
        '''
        List of users member of a space

        Args:
            space: space identifier
        Returns:
            list of users

              {'users': [...]}
        '''
        headers = {
            'X-PFM-PLUGIN': self.name,
            'X-PFM-PLUGINSECRET': self.token(),
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
        r = requests.post(
            '%s/internal/api/pfm/space/%d/admin/module/user/list_users' % (
                self.pfm,
                space
            ),
            json={},
            headers=headers
        )
        if r.status_code != 200:
            self.logger.error('[plugin=%s][space=%d] list_users error %d: %s' % (
                self.name,
                space,
                r.status_code,
                r.text
            ))
            return None
        users_data = r.json()
        return users_data

    def get_space(self, space:int) -> dict:
        '''
        Gets space information

        Args:
            space: space identifier
        Returns:
            space object

              {'space': {'id': xx, ...}}
        '''
        headers = {
            'X-PFM-PLUGIN': self.name,
            'X-PFM-PLUGINSECRET': self.token(),
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
        r = requests.post(
            '%s/internal/api/pfm/space/%d/admin/module/space/get_space' % (
                self.pfm,
                space
            ),
            json={'space':{'id':space}},
            headers=headers
        )
        if r.status_code != 200:
            self.logger.error('[plugin=%s][space=%d] get_space error %d: %s' % (
                self.name,
                space,
                r.status_code,
                r.text
            ))
            return None
        space_data = r.json()
        return space_data

    def get_customer(self, space:int, uid:int) -> dict:
        '''
        Gets user customer in space

        Args:
            space: space identifier
            uid: user identifier
        Returns:
            customer information

              {'customer': [...]}
        '''
        headers = {
            'X-PFM-PLUGIN': self.name,
            'X-PFM-PLUGINSECRET': self.token(),
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
        r = requests.post(
            '%s/internal/api/pfm/space/%d/admin/module/user/get_customer' % (
                self.pfm,
                space
            ),
            json={'user':{'id':uid}},
            headers=headers
        )
        if r.status_code != 200:
            self.logger.error('[plugin=%s][space=%d] get_customer error %d: %s' % (
                self.name,
                space,
                r.status_code,
                r.text
            ))
            return None
        customer_data = r.json()
        return customer_data

    def get_user_role(self, space:int, uid:int) -> dict:
        '''
        Gets user role in space

        Args:
            space: space identifier
            uid: user identifier
        Returns:
            role information

              {'role': {'id': xx, ...}}
        '''
        headers = {
            'X-PFM-PLUGIN': self.name,
            'X-PFM-PLUGINSECRET': self.token(),
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
        r = requests.post(
            '%s/internal/api/pfm/space/%d/admin/module/user/get_role' % (
                self.pfm,
                space
            ),
            json={'user':{'id':uid}},
            headers=headers
        )
        if r.status_code != 200:
            self.logger.error('[plugin=%s][space=%d] get_customer error %d: %s' % (
                self.name,
                space,
                r.status_code,
                r.text
            ))
            return None
        role_data = r.json()
        return role_data

    def get_user(self, space:int, uid:int=0, name:str='') -> dict:
        '''
        Gets user info

        Either `uid` or `name` parameter must be provided

        Args:
            space: space identifier
            uid (optional): user identifier
            name (optional): user name
        Returns:
            user information

              {'user': {'id': ...}}
        '''
        headers = {
            'X-PFM-PLUGIN': self.name,
            'X-PFM-PLUGINSECRET': self.token(),
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
        user = {'user':{'id':uid, 'name': name}}
        r = requests.post(
            '%s/internal/api/pfm/space/%d/admin/module/user/get_user' % (
                self.pfm,
                space
            ),
            json=user,
            headers=headers
        )
        if r.status_code != 200:
            self.logger.error('[plugin=%s][space=%d] get_user error %d: %s' % (
                self.name,
                space,
                r.status_code,
                r.text
            ))
            return None
        user_data = r.json()
        return user_data

    def __plan_enabled(self, space: int):
        headers = {
            'X-PFM-PLUGIN': self.name,
            'X-PFM-PLUGINSECRET': self.token(),
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
        r = requests.post(
            '%s/internal/api/pfm/space/%d/admin/module/space/get_plan' % (
                self.pfm,
                space
            ),
            json={'space':{'id':space}},
            headers=headers
        )
        if r.status_code != 200:
            self.logger.error('[plugin=%s][space=%d] get_plan error %d: %s' % (
                self.name,
                space,
                r.status_code,
                r.text
            ))
            return False
        plan = r.json()
        if self.name not in plan['plan'].get('modules', None):
            return False
        return True

    def callback(self, ch, method, properties, body):
        '''
        Callback function called by pika library on rabbitmq message

        On message, `event` field of `body` is tested against a plugin
        method if it exists, else message is just acked.

        If method exists, method is called with message `data` field.

        Examples:
           message {'event': 'test_msg', 'data': {'test': 1}} arrives
           callback with see if self.test_msg method exists
           and calls self.test_msg(data)

        '''
        self.logger.debug(" [x] %r" % body)
        data = json.loads(body)
        if data['data']['logged'].get('customer', None) is None:
            data['data']['logged']['customer'] = {'id': 0}
        if not self.__plan_enabled(data['data']['logged']['space']):
            self.logger.debug('[plugin=%s][space=%d] not active for space' % (
                self.name,
                data['data']['logged']['space']
            ))
            ch.basic_ack(delivery_tag=method.delivery_tag)
            return

        event = data.get('event', None)
        if not event:
            self.logger.debug('[plugin=%s][space=%d] missing event' % (
                self.name,
                data['data']['logged']['space']
            ))
            ch.basic_ack(delivery_tag=method.delivery_tag)
            return
        self.logger.debug('[plugin=%s][space=%d][event=%s] call' % (
            self.name,
            data['data']['logged']['space'],
            event
        ))

        if hasattr(self, event):
            try:
                todo = getattr(self, event)
                todo(data.get('data', None))
            except Exception as e:
                self.logger.exception('[plugin=%s][event=%s] plugin failure: %s' % (
                    self.name,
                    event,
                    str(e)
                ))
        else:
            self.logger.debug('[plugin=%s][space=%d][event=%s] ignore event' % (
                self.name,
                data['data']['logged']['space'],
                event
            ))
        ch.basic_ack(delivery_tag=method.delivery_tag)
        if self.should_terminate:
            ch.basic_cancel(self.consumer_tag)

    def run(self):
        '''
        Entrypoint of a plugin

        Connect and consume messages from rabbitmq queue
        '''
        signal.signal(signal.SIGTERM, self.exit_gracefully)
        self.consumer_tag = self.channel.basic_consume(
            queue=self.plugin_queue.method.queue,
            on_message_callback=self.callback,
            auto_ack=False)

        self.channel.start_consuming()

    def token(self):
        '''
        Generate a jwt token

        Returns:
          string: a JWT encoded token with plugin name
        '''
        secret = jwt.encode({'plugin': self.name}, self.secret)
        return secret
